#!/usr/bin/python

import base64
import argparse

def encode(s):
    return base64.b64encode(s)

def decode(s):
    try:
        return base64.b64decode(s)
    except:
        return '<error>'

def main(args):
    strings = args.strings

    if strings == None:
        print 'Please specifiy a string or strings with -s'
        return

    print 'Original \t Encoding \t Decoding'

    print ''

    # Try all strings of length 1
    for s in strings:
        print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

    print '\n----------------------\n'

    # Try all strings of length 2
    if len(strings)<2:
        return

    delimiters = ['', '.', '_', '-']
    for s1 in strings:
        for s2 in strings:
            if s1==s2:
                continue

            for d in delimiters:
                s = s1 + d + s2
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

            s = s1[0] + s2
            print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

            s = s1 + s2[0]
            print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

            s = s1[0] + s2[0]
            print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

    print '\n----------------------\n'

    # Try all strings of length 3
    if len(strings)<3:
        return

    delimiters = ['', '.', '_', '-']
    for s1 in strings:
        for s2 in strings:
            for s3 in strings:
                if s1==s2 or s2==s3 or s1==s3:
                    continue

                for d in delimiters:
                    s = s1 + d + s2 + d + s3
                    print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                # Now try initials

                s = s1[0] + s2 + s3
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                s = s1 + s2[0] + s3
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                s = s1[0] + s2[0] + s3
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                s = s1[0] + s2 + s3[0]
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                s = s1 + s2[0] + s3[0]
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

                s = s1[0] + s2[0] + s3[0]
                print '%s       \t %s       \t %s' % (s, encode(s), decode(s))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--strings', nargs='+', type=str)
    args = parser.parse_args()

    main(args)

